#!/usr/bin/env python

import numpy as np

from pycrazyswarm import *
import uav_trajectory

crazyflies_yaml = """
crazyflies:
  - channel: 100
    id: 2
    initialPosition: [-1.35, 0.45, 0.006]
    type: default
"""

if __name__ == "__main__":
    swarm = Crazyswarm(crazyflies_yaml=crazyflies_yaml)
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    # traj1 = uav_trajectory.Trajectory()
    # traj1.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj1.csv")

    # traj2 = uav_trajectory.Trajectory()
    # traj2.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj2.csv")

    traj1 = uav_trajectory.Trajectory()
    traj1.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj2.csv")

    # traj4 = uav_trajectory.Trajectory()
    # traj4.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj4.csv")

    # traj5 = uav_trajectory.Trajectory()
    # traj5.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj5.csv")

    # traj6 = uav_trajectory.Trajectory()
    # traj6.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj6.csv")

    TRIALS = 1
    TIMESCALE = 1.0
    for i in range(TRIALS):
        allcfs.crazyflies[0].uploadTrajectory(0,0,traj1)

        allcfs.takeoff(targetHeight=0.7, duration=3.5)
        timeHelper.sleep(4.0)

        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0, 0, 0.7])
            cf.goTo(pos, 0, 6.0)
        timeHelper.sleep(6.5)


        allcfs.startTrajectory(0, timescale=TIMESCALE)
        timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)
        
        #allcfs.startTrajectory(0, timescale=TIMESCALE, reverse=True)
        #timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)

        allcfs.land(targetHeight=0.06, duration=3)
        timeHelper.sleep(3.5)

