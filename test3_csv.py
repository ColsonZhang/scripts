#!/usr/bin/env python

import numpy as np

from pycrazyswarm import *
import uav_trajectory

crazyflies_yaml = """
    crazyflies:
  - channel: 100
    id: 1
    initialPosition: [-1.35, 1.35, 0.006]
    type: default
  - channel: 80
    id: 6
    initialPosition: [-0.9, 1.35, 0.006]
    type: default
  - channel: 80
    id: 3
    initialPosition: [-1.35, 0.9, 0.006]
    type: default
  - channel: 100
    id: 4
    initialPosition: [-0.9, 0.9, 0.006]
    type: default
    """



"""
crazyflies:
- channel: 100
  id: 1
  initialPosition: [-1.0, 0.0, 0.006]
  type: default
- channel: 100
  id: 2
  initialPosition: [-0.5, 0.0, 0.006]
  type: default
- channel: 80
  id: 3
  initialPosition: [ -1.0, -0.5, 0.006]
  type: default
- channel: 100
  id: 5
  initialPosition: [-0.5, -0.5, 0.006]
  type: default
"""




if __name__ == "__main__":
    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    traj1 = uav_trajectory.Trajectory()
    traj1.loadcsv("./traj/trajectory_new/4RobotsSquareFormation_500/traj1.csv")

    traj2 = uav_trajectory.Trajectory()
    traj2.loadcsv("./traj/trajectory_new/4RobotsSquareFormation_500/traj2.csv")

    traj3 = uav_trajectory.Trajectory()
    traj3.loadcsv("./traj/trajectory_new/4RobotsSquareFormation_500/traj3.csv")

    traj4 = uav_trajectory.Trajectory()
    traj4.loadcsv("./traj/trajectory_new/4RobotsSquareFormation_500/traj4.csv")

    TRIALS = 1
    TIMESCALE = 1.0
    for i in range(TRIALS):

        allcfs.crazyflies[0].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[1].uploadTrajectory(0,0,traj2)
        allcfs.crazyflies[2].uploadTrajectory(0,0,traj3)
        allcfs.crazyflies[3].uploadTrajectory(0,0,traj4)


        allcfs.takeoff(targetHeight=1.0, duration=3.0)
        timeHelper.sleep(3.5)
        
        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0, 0, 1.0])
            cf.goTo(pos, 0, 5.0)
        timeHelper.sleep(6)

        allcfs.startTrajectory(0, timescale=TIMESCALE)
        timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)
        #allcfs.startTrajectory(0, timescale=TIMESCALE, reverse=True)
        #timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)

        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0.0, 0.0, 1.0])
            cf.goTo(pos, 0, 10.0)
        timeHelper.sleep(11)

        allcfs.land(targetHeight=0.06, duration=2.0)
        timeHelper.sleep(3.0)
