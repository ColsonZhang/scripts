#!/usr/bin/env python

import numpy as np

from pycrazyswarm import *
import uav_trajectory


if __name__ == "__main__":
    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    traj1 = uav_trajectory.Trajectory()
    traj1.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj1.csv")

    traj2 = uav_trajectory.Trajectory()
    traj2.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj2.csv")

    traj3 = uav_trajectory.Trajectory()
    traj3.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj3.csv")

    traj4 = uav_trajectory.Trajectory()
    traj4.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj4.csv")

    traj5 = uav_trajectory.Trajectory()
    traj5.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj5.csv")

    traj6 = uav_trajectory.Trajectory()
    traj6.loadcsv("./traj/trajectory_new/6RobotsCorridorFormationChange_10s_500/traj6.csv")

    TRIALS = 1
    TIMESCALE = 1.0
    for i in range(TRIALS):
        allcfs.crazyflies[0].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[1].uploadTrajectory(0,0,traj2)
        allcfs.crazyflies[2].uploadTrajectory(0,0,traj3)
        allcfs.crazyflies[3].uploadTrajectory(0,0,traj4)
        allcfs.crazyflies[4].uploadTrajectory(0,0,traj5)
        allcfs.crazyflies[5].uploadTrajectory(0,0,traj6)
        


        allcfs.takeoff(targetHeight=0.5, duration=3.5)
        timeHelper.sleep(4)

        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0, 0, 0.5])
            cf.goTo(pos, 0, 8.0)
        timeHelper.sleep(8.5)

        allcfs.startTrajectory(0, timescale=TIMESCALE)
        timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)
        #allcfs.startTrajectory(0, timescale=TIMESCALE, reverse=True)
        #timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)

        # for i in range(6):
        #   cf = allcfs.crazyflies[i]
        #   if i%2 == 1:
        #     pos = np.array(cf.initialPosition) + np.array([1.5, 0.0, 1.0])
        #     cf.goTo(pos, 0, 8.0)
        #   elif i%2 == 0:
        #     pos = np.array(cf.initialPosition) + np.array([1.5, 0.4, 1.0])
        #     cf.goTo(pos, 0, 8.0)
        # timeHelper.sleep(9.0)

        allcfs.land(targetHeight=0.06, duration=2.0)
        timeHelper.sleep(2.5) 

        """ 
        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([1.5, 1.0, 1.0])
            cf.goTo(pos, 0, 10.0)
        timeHelper.sleep(11)
        """
