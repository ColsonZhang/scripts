#!/usr/bin/env python

import numpy as np

from pycrazyswarm import *
import uav_trajectory


if __name__ == "__main__":
    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    traj1 = uav_trajectory.Trajectory()
    traj1.loadcsv("figure8.csv")

    TRIALS = 1
    TIMESCALE = 1.0
    for i in range(TRIALS):
        allcfs.crazyflies[0].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[1].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[2].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[3].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[4].uploadTrajectory(0,0,traj1)
        allcfs.crazyflies[5].uploadTrajectory(0,0,traj1)

        allcfs.takeoff(targetHeight=1.0, duration=3.5)
        timeHelper.sleep(4.0)

        # for cf in allcfs.crazyflies:
        #     pos = np.array(cf.initialPosition) + np.array([0, 0, 1])
        #     cf.goTo(pos, 0, 3.0)
        # timeHelper.sleep(3.5)


        allcfs.startTrajectory(0, timescale=TIMESCALE)
        timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)
        
        #allcfs.startTrajectory(0, timescale=TIMESCALE, reverse=True)
        #timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)

        allcfs.land(targetHeight=0.06, duration=3)
        timeHelper.sleep(3.5)

