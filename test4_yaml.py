crazyflies_yaml = """
crazyflies:
  - channel: 80
    id: 1
    initialPosition: [-1.8, 0.45, 0.006]
    type: default
  - channel: 100
    id: 2
    initialPosition: [-1.35, 0.45, 0.006]
    type: default
  - channel: 120
    id: 3
    initialPosition: [-1.8, 0.0, 0.006]
    type: default
  - channel: 110
    id: 4
    initialPosition: [-1.35, -0.0, 0.006]
    type: default
  - channel: 100
    id: 5
    initialPosition: [-1.8, -0.45, 0.006]
    type: default
  - channel: 120
    id: 6
    initialPosition: [-1.35, -0.45, 0.006]
    type: default
"""
