#!/usr/bin/env python

import numpy as np

from pycrazyswarm import *
import uav_trajectory


if __name__ == "__main__":
    swarm = Crazyswarm()
    timeHelper = swarm.timeHelper
    allcfs = swarm.allcfs

    TRIALS = 1
    TIMESCALE = 1.0

    trajs = [np.array([1,0,2]),np.array([1.5,0,2]),np.array([2,0,2]),np.array([2.5,0,2])]

    for i in range(TRIALS):

        allcfs.takeoff(targetHeight=1.0, duration=3.0)
        timeHelper.sleep(3.5)
        
        for i in range(len(trajs)):
            for cf in allcfs.crazyflies:
                pos = np.array(cf.initialPosition) + trajs[i]
                cf.goTo(pos, 0, 5.0)
            timeHelper.sleep(5.5)            

        """
            for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0, 0, 1.0])
            cf.goTo(pos, 0, 5.0)
        timeHelper.sleep(6) """

        # allcfs.startTrajectory(0, timescale=TIMESCALE)
        # timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)
        #allcfs.startTrajectory(0, timescale=TIMESCALE, reverse=True)
        #timeHelper.sleep(traj1.duration * TIMESCALE + 2.0)

        for cf in allcfs.crazyflies:
            pos = np.array(cf.initialPosition) + np.array([0.0, 0.0, 1.0])
            cf.goTo(pos, 0, 10.0)
        timeHelper.sleep(11)

        allcfs.land(targetHeight=0.06, duration=2.0)
        timeHelper.sleep(3.0)
